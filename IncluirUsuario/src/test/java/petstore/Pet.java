package petstore;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.contains

public class Pet {
	
	String uri = "https://reqres.in/"; 
	
	public String lerJason(String caminhoJason) throws IOException {
		
		return new String (Files.readAllBytes(Paths.get(caminhoJason)));
	}
	
	@Test(priority=1)
	public void incluirUsuario() throws IOException {
		String jsonBody = lerJason("src/main/java/db/incluir.json");
		
		given() 
        .contentType("application/json")
        .log().all()
        .body(jsonBody)
.when()  
        .post(uri)
.then()  
        .log().all()
        .statusCode(200)
        .body("email" , is("george.bluth@reqres.in"))
 ;
	}

	@Test(priority=2)
	public void consultarUsuario() {
		String id = "1";
		given()
		       .contentType("application/json")
		       .log(). all()
		.when()    
		       .get(uri + "/" + id)
		.then()
		       .log(). all()
		       .statusCode(200)
		       .body("email" , is("george.bluth@reqres.in"))
		;       
	}
		
	@Test(priority=3)
	public void excluirUsuario() throws IOException {
		String id = "2";
		given()
		       .contentType("application/json")
		       .log(). all()
		.when()    
		       .delete(uri + "/" + id)
		.then()
		       .log(). all()
		       .statusCode(200)
		       .body("code" , is("204"))
		;    
	}
		
	
}
